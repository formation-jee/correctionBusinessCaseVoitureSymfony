<?php

namespace App\Entity;

use App\Repository\MarqueRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=MarqueRepository::class)
 */
class Marque
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotNull(message="Veuillez saisir un nom pour la marque")
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity=Car::class, mappedBy="marque", orphanRemoval=true)
     */
    private $cars;

    /**
     * @ORM\OneToOne(targetEntity=Image::class, mappedBy="marque", cascade={"persist", "remove"})
     */
    private $logo;

    public function __construct()
    {
        $this->cars = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Car[]
     */
    public function getCars(): Collection
    {
        return $this->cars;
    }

    public function addCar(Car $car): self
    {
        if (!$this->cars->contains($car)) {
            $this->cars[] = $car;
            $car->setMarque($this);
        }

        return $this;
    }

    public function removeCar(Car $car): self
    {
        if ($this->cars->removeElement($car)) {
            // set the owning side to null (unless already changed)
            if ($car->getMarque() === $this) {
                $car->setMarque(null);
            }
        }

        return $this;
    }

    public function getLogo(): ?Image
    {
        return $this->logo;
    }

    public function setLogo(?Image $logo): self
    {
        // unset the owning side of the relation if necessary
        if ($logo === null && $this->logo !== null) {
            $this->logo->setMarque(null);
        }

        // set the owning side of the relation if necessary
        if ($logo !== null && $logo->getMarque() !== $this) {
            $logo->setMarque($this);
        }

        $this->logo = $logo;

        return $this;
    }

    public function __toString(): string
    {
        return $this->name;
    }
}
