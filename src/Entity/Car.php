<?php

namespace App\Entity;

use App\Repository\CarRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CarRepository::class)
 */
class Car
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(
     *      min = 2,
     *      max = 5,
     *      minMessage = "Veuillez saisir au moins {{ limit }} caractere",
     *      maxMessage = "Vous avez {{ limit }} caractere. C'est trop"
     * )
     */
    private $model;

    /**
     * @ORM\Column(type="boolean")
     */
    private $boiteAuto;

    /**
     * @ORM\ManyToOne(targetEntity=Energy::class, inversedBy="cars", fetch="EAGER")
     * @ORM\JoinColumn(nullable=false)
     */
    private $energy;

    /**
     * @ORM\ManyToOne(targetEntity=Marque::class, inversedBy="cars", fetch="EAGER")
     * @ORM\JoinColumn(nullable=false)
     */
    private $marque;

    /**
     * @ORM\OneToMany(targetEntity=Image::class, mappedBy="car", fetch="EAGER", cascade={"remove", "persist"})
     * @Assert\Valid()
     */
    private $images;

    public function __construct()
    {
        $this->images = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getModel(): ?string
    {
        return $this->model;
    }

    public function setModel(string $model): self
    {
        $this->model = $model;

        return $this;
    }

    public function getBoiteAuto(): ?bool
    {
        return $this->boiteAuto;
    }

    public function setBoiteAuto(bool $boiteAuto): self
    {
        $this->boiteAuto = $boiteAuto;

        return $this;
    }

    public function getEnergy(): ?Energy
    {
        return $this->energy;
    }

    public function setEnergy(?Energy $energy): self
    {
        $this->energy = $energy;

        return $this;
    }

    public function getMarque(): ?Marque
    {
        return $this->marque;
    }

    public function setMarque(?Marque $marque): self
    {
        $this->marque = $marque;

        return $this;
    }

    /**
     * @return Collection|Image[]
     */
    public function getImages(): Collection
    {
        return $this->images;
    }

    public function addImage(Image $image): self
    {
        if (!$this->images->contains($image)) {
            $this->images[] = $image;
            $image->setCar($this);
        }

        return $this;
    }

    public function removeImage(Image $image): self
    {
        if ($this->images->removeElement($image)) {
            // set the owning side to null (unless already changed)
            if ($image->getCar() === $this) {
                $image->setCar(null);
            }
        }

        return $this;
    }

    public function __toString(){
        return $this->model;
    }

}
