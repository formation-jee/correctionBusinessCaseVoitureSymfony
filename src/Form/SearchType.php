<?php


namespace App\Form;

use App\Entity\Car;
use App\Entity\Energy;
use App\Entity\Marque;
use Doctrine\ORM\Mapping\Entity;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SearchType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('searchText', TextType::class, [
                'required'=> false,
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('energy', EntityType::class, [
                'required'=> false,
                'class'=> Energy::class,
                'expanded'=> true,
                'multiple'=> true
            ])
            ->add('marque', EntityType::class, [
                'required'=> false,
                'class'=> Marque::class
            ])

            ->add('enregistrer', SubmitType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => null,
        ]);
    }
}

?>