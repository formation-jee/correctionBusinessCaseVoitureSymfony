<?php

namespace App\Form;

use App\Entity\Car;
use App\Entity\Energy;
use App\Entity\Marque;
use Doctrine\ORM\Mapping\Entity;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\All;
use Symfony\Component\Validator\Constraints\File;

class CarType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('model', TextType::class, [
                'required'=> false,
                'attr'=> [
                    'class'=> 'form-control'
                ]
            ])
            ->add('boiteAuto', CheckboxType::class, [
                'required'=>false,
                'attr'=> [
                    'class'=> 'form-check-input'
                ]
            ])
            ->add('energy', EntityType::class, [
                'class'=> Energy::class,
                'label_attr'=> [
                    'class'=> "label"
                ],
                'attr'=> [
                    'class'=> 'form-control'
                ]
            ])
            ->add('marque',EntityType::class, [
                'class'=> Marque::class,
                'expanded'=>true,
                'multiple'=>false
            ])
            ->add('images', FileType::class, [
                'multiple'=> true,
                'mapped'=> false,
                'attr'     => [
                    'accept'=> 'image/*',
                    'multiple' => 'multiple'
                ],
                'constraints' => [
                    new All(
                        new File([
                            'maxSize' => '1024k',
                            'mimeTypes' => [
                                'image/jpeg',
                                'image/png',
                            ],
                            'mimeTypesMessage' => 'Please upload a valid PDF document',
                        ])
                    )
                ]
            ])
            ->add('enregistrer', SubmitType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Car::class,
        ]);
    }
}
