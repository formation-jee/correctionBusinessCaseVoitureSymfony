<?php
namespace App\Services;

use Doctrine\ORM\EntityManagerInterface;

class LoggerService {


    public function logHello(){
        dump('Hello') ;
    }

    public function logControllerName($name){
        dump($name);
    }

    public function logFunctionName($function){
        dump($function);
    }
}