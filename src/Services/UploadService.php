<?php
namespace App\Services;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\String\Slugger\SluggerInterface;

class UploadService{
    private $slugger;
    private $directory;

    public function __construct(SluggerInterface $slugger,
    ContainerInterface $container){
        $this->slugger = $slugger;
        $this->directory = $container->getParameter('images_directory');
    }

    public function uploadFile($file){
        $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        $safeFilename = $this->slugger->slug($originalFilename);
        $newFilename = $safeFilename.'-'.uniqid().'.'.$file->guessExtension();
        try {
            $file->move(
                $this->directory,
                $newFilename
            );

            return $newFilename;
        } catch (FileException $e) {
            dump($e);
            die();
        }


    }
}