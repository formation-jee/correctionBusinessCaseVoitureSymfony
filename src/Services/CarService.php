<?php
namespace App\Services;

use App\Entity\Car;
use App\Repository\CarRepository;
use Doctrine\ORM\EntityManagerInterface;

class CarService {
    private $em;
    private $carRepository;

    public function __construct(EntityManagerInterface $em,
                                CarRepository $carRepository){
        $this->em = $em;
        $this->carRepository = $carRepository;
    }

    public function add(Car $car){
        foreach ($car->getImages() as $image) {
            $car->addImage($image);
            $image->setCar($car);
        }
        $this->em->persist($car);
        $this->em->flush();
    }

    public function getOne($id){
        return $this->carRepository->find($id);
    }

    public function getAll(){
        return $this->carRepository->findAll();
    }

    public function delete(Car $car){

        $this->em->remove($car);
        $this->em->flush();
    }

    public function edit(Car $car){
        $this->em->flush();
    }
}