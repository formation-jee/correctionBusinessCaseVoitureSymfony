<?php

namespace App\Command;

use App\Entity\Energy;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'app:create-energy',
    description: 'Commande qui cré une nouvelle energy',
)]
class CreateEnergyCommand extends Command
{
    private $em;

    public function __construct(EntityManagerInterface $em,string $name = null)
    {
        parent::__construct($name);
        $this->em = $em;
    }

    protected function configure(): void
    {
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $helper = $this->getHelper('question');
        $question = new ConfirmationQuestion('Continue with this action?', false);



        $questionEnergy = new Question("Quelle est le nom de l'energy : ");
        $resultEnergy = $helper->ask($input, $output, $questionEnergy);


        $result = $helper->ask($input,$output, $question);

        if($result){
            $energy = new Energy();
            $energy->setType($resultEnergy);
            $this->em->persist($energy);
            $this->em->flush();
            $output->write("Félicitation !!");
            return Command::SUCCESS;
        } else {
            $output->writeln("Opération annulée !!");
            return Command::FAILURE;
        }
    }
}
