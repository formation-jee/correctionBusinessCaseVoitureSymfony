<?php

namespace App\DataFixtures;

use App\Entity\Car;
use App\Services\CarService;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class CarFixtures extends Fixture implements DependentFixtureInterface
{
    private $carService;

    public function __construct(CarService $carService){
        $this->carService = $carService;
    }

    public function load(ObjectManager $manager)
    {
        $cars = [
            [
                'model' => 'Model S',
                'boiteAuto' => true,
                'marque' => 'tesla',
                'energy' => 'electrique',
                'images' => [
                    'tesla-1',
                    'tesla-2',
                    'tesla-3'
                ]
            ],
            [
                'model' => 'Clio',
                'boiteAuto' => false,
                'marque' => 'renault',
                'energy' => 'essence',
                'images' => [
                    'renault-clio-1',
                    'renault-clio-2'
                ]
            ]
        ];

        foreach ($cars as $car){
            $object = new Car();
            $object->setBoiteAuto($car['boiteAuto']);
            $object->setModel($car['model']);
            $object->setMarque($this->getReference($car['marque']));
            $object->setEnergy($this->getReference($car['energy']));

            foreach ($car['images'] as $image){
                $object->addImage($this->getReference($image));
            }

            $this->carService->add($object);
        }


        // $product = new Product();
        // $manager->persist($product);

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            MarqueFixtures::class,
            EnergyFixtures::class,
            ImageFixtures::class
        ];
    }
}
