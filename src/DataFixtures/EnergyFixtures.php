<?php

namespace App\DataFixtures;

use App\Entity\Energy;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class EnergyFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $energys = ['essence', 'diesel',
            'hybride', 'electrique'];

        foreach ($energys as $energyString){
            $energy = new Energy();
            $energy->setType($energyString);
            $manager->persist($energy);

            $this->addReference($energyString, $energy);

        }

        $manager->flush();
    }
}
