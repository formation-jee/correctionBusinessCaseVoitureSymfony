<?php

namespace App\DataFixtures;

use App\Entity\Image;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class ImageFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // Images relatives aux logos des marques
       $images = [
           ['marque'=> 'renault', 'url'=>"https://www.largus.fr/images/images/logo-renault-fond-noir.jpg"],
           ['marque'=> 'citroen', 'url'=>  "https://cdn.1min30.com/wp-content/uploads/2017/08/Logo-Citroën-1.jpg"],
           ['marque'=> 'tesla', 'url'=> "http://www.logo-voiture.com/wp-content/uploads/2021/01/tesla-logo-2200x2800-grand.png"]
       ];
       foreach ($images as $image){
           $object = new Image();
           $object->setUrl($image['url']);
           $manager->persist($object);

           $this->addReference($image['marque'].'-logo', $object);
       }

        // Images relatives aux vehicules

        $images = [
            [
            'reference'=> 'tesla-1',
                'url'=> 'https://www.largus.fr/images/images/tesla-model-3_11.jpg'
            ],

            [
                'reference'=> 'tesla-2',
                'url'=> 'https://www.turbo.fr/sites/default/files/styles/article_690x405/public/2021-07/Tesla.jpeg?itok=elY2jPWi'
            ],

            [
                'reference'=> 'tesla-3',
                'url'=> 'https://www.automobile-magazine.fr/asset/cms/179753/config/128461/am-tesla-model-3-11.jpg'
            ],
            [
                'reference'=> 'renault-clio-1',
                'url'=> 'https://www.largus.fr/images/images/renault-clio-limited_1.jpg?width=612&quality=80'
            ],
            [
            'reference'=> 'renault-clio-2',
            'url'=> 'https://upload.wikimedia.org/wikipedia/commons/thumb/5/59/Renault_Clio_V_Genf_2019_1Y7A5590.jpg/1200px-Renault_Clio_V_Genf_2019_1Y7A5590.jpg'
        ]
        ];

       foreach ($images as $image){
           $object = new Image();
           $object->setUrl($image['url']);
           $manager->persist($object);
           $this->addReference($image['reference'], $object);
       }

        $manager->flush();
    }
}
