<?php

namespace App\DataFixtures;

use App\Entity\Marque;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class MarqueFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $marques = ['Renault', 'Citroen', 'Tesla'];

        foreach ($marques as $marque){
            $object = new Marque();
            $object->setName($marque);
            $object->setLogo($this->getReference(strtolower($marque).'-logo'));
            $manager->persist($object);

            $this->addReference(strtolower($marque), $object);
        }
        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            ImageFixtures::class
        ];
    }
}
