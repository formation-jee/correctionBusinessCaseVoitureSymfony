<?php
namespace App\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class OnlyLetter extends Constraint {
    public $message = 'Impossible : "{{ string }}" ne contient pas que des lettres.';

    public function validatedBy()
    {
        return static::class.'Validator';
    }
}