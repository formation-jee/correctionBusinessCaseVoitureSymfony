<?php

namespace App\Repository;

use App\Entity\Car;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Car|null find($id, $lockMode = null, $lockVersion = null)
 * @method Car|null findOneBy(array $criteria, array $orderBy = null)
 * @method Car[]    findAll()
 * @method Car[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CarRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Car::class);
    }

    public function searchEngine($filtres){
        $objectArray = $this->createQueryBuilder('c')
        ->join('c.marque', 'm')
        ->join('c.energy', 'e');
        if(!is_null($filtres['searchText'])){
            $objectArray->andWhere('c.model LIKE :model')
                ->setParameter('model', '%'.$filtres['searchText'].'%');
        }
        if(!is_null($filtres['energy'])){
            $objectArray->andWhere('e IN (:energy)');
            $objectArray->setParameter('energy', $filtres['energy']);
        }
        if(!is_null($filtres['marque'])){
            $objectArray->andWhere('m = :marque')
                ->setParameter('marque', $filtres['marque']);
        }
        return $objectArray->getQuery()->getResult();
    }


    /*
    public function findOneBySomeField($value): ?Car
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
