<?php

namespace App\Controller;

use App\Form\SearchType;
use App\Repository\CarRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SearchController extends AbstractController
{
    private $carRepository;

    public function __construct(CarRepository $carRepository){
        $this->carRepository = $carRepository;
    }

    #[Route('/search', name: 'search')]
    public function index(Request $request): Response
    {
        $form = $this->createForm(SearchType::class);
        $form->handleRequest($request);
        $results = $this->carRepository->findAll();

        if($form->isSubmitted() && $form->isValid()){
            // ICI j'irais rechercher
            $results = $this->carRepository->searchEngine($form->getData());
        }
        return $this->render('search/index.html.twig', [
            'form' => $form->createView(),
            'results'=> $results
        ]);
    }
}
