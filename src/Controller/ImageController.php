<?php

namespace App\Controller;

use App\Entity\Image;
use App\Form\ImageType;
use App\Repository\ImageRepository;
use App\Services\UploadService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;

#[Route('/image', name: 'image_')]
class ImageController extends AbstractController
{
    private $imageRepository;
    private $em;

    private $uploadService;

    public function __construct(ImageRepository $imageRepository, EntityManagerInterface $em,
    UploadService $uploadService){
        $this->imageRepository = $imageRepository;
        $this->em = $em;

        $this->uploadService = $uploadService;
    }

    #[Route('/', name: 'list')]
    public function index(): Response
    {
        $images = $this->imageRepository->findAll();

        return $this->render('image/index.html.twig', [
            'images' => $images
        ]);
    }

    #[Route('/add', name: 'add')]
    public function add(Request $request){
        $form = $this->createForm(ImageType::class, new Image());

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $image = $form->get('url')->getData();

            $filename = $this->uploadService->uploadFile($image);

            $image = $form->getData();
            $image->setUrl($filename);

            $this->em->persist($image);
            $this->em->flush();

            return $this->redirectToRoute("image_list");
        }
        return $this->render('image/form.html.twig', [
            'form'=> $form->createView()
        ]);
    }
}
