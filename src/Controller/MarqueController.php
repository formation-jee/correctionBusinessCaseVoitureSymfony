<?php

namespace App\Controller;

use App\Entity\Marque;
use App\Repository\MarqueRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/marque', name: 'marque_')]
class MarqueController extends AbstractController
{
    private $marqueRepo;

    public function __construct(MarqueRepository $marqueRepo)
    {
        $this->marqueRepo = $marqueRepo;
    }

    #[Route('/list', name: 'list')]
    public function index(): Response
    {
        $marques = $this->marqueRepo->findAll();

        return $this->render('marque/index.html.twig', [
            'marques' => $marques
        ]);
    }

    #[Route('/{marque}', name: 'detail')]
    public function detail(Marque $marque): Response
    {
        return $this->render('marque/detail.html.twig',
        [
            'marque'=> $marque
        ]);
    }
}
