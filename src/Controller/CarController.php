<?php

namespace App\Controller;

use App\Entity\Car;
use App\Entity\Image;
use App\Form\CarType;
use App\Repository\CarRepository;
use App\Services\CarService;
use App\Services\LoggerService;
use App\Services\UploadService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/car', name: 'car_')]
class CarController extends AbstractController
{

    private $carService;
    private $uploadService;

    public function __construct(CarService $carService,
                                UploadService $uploadService) {
        $this->carService = $carService;
        $this->uploadService = $uploadService;
    }

    #[Route('/list', name: 'list')]
    public function index(): Response
    {
        $cars = $this->carService->getAll();
        return $this->render('car/index.html.twig', [
            'cars' => $cars,
        ]);
    }

    #[Route('/{car}', name: 'detail', requirements: ['car' => '\d+'])]
    public function detail(Car $car): Response{
        return $this->render('car/detail.html.twig', [
            'car'=> $car
        ]);
    }
/*
    #[Route('/update/{car}', name: 'update')]
    public function update(Car $car): Response{
        $car->setModel('toto');
        $car->setBoiteAuto(false);
        $this->em->flush();

        return $this->render('car/update.html.twig', [
            'car'=> $car
        ]);
    }*/

    #[Route('/delete/{car}', name: 'delete')]
    public function delete(Car $car, Request $request){
        $session = $request->getSession();
        $session->getFlashBag()->add('notice', $car->getModel().' '
            .$car->getMarque()->getName(). ' supprimé');

       $this->carService->delete($car);

        return $this->redirectToRoute('car_list');
    }

    #[Route('/add',name: 'add')]
    public function create(Request $request): Response {
        $form = $this->createForm(CarType::class, new Car());

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $car = $form->getData();

            $images = $form->get('images')->getData();

            foreach ($images as $image){
                $filename = $this->uploadService->uploadFile($image);
                $imageToAdd = new Image();
                $imageToAdd->setUrl($filename);
                $imageToAdd->setName($image->getClientOriginalName());
                $imageToAdd->setCar($car);
                $car->addImage($imageToAdd);
            }

            $this->carService->add($car);

            $session = $request->getSession();
            $session->getFlashBag()->add('notice', $car->getModel().' '
                .$car->getMarque()->getName(). ' ajouté !');
            return $this->redirectToRoute('car_list');
        }


        return $this->render('car/form.html.twig', [
           'form'=>$form->createView()
        ]);
    }

    #[Route('/update/{car}', name: 'update')]
    public function update(Car $car, Request $request): Response {
        $form = $this->createForm(CarType::class, $car);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $car = $form->getData();
            $this->carService->edit($car);
            $session = $request->getSession();
            $session->getFlashBag()->add('notice', $car->getModel().' '
                .$car->getMarque()->getName(). ' édité !');
            return $this->redirectToRoute('car_list');
        }

        return $this->render('car/form.html.twig', [
            'form'=>$form->createView()
        ]);
    }


}
